Categories:Internet
License:GPLv3+
Web Site:https://github.com/scoute-dich/Diaspora/blob/HEAD/README.md
Source Code:https://github.com/scoute-dich/Diaspora
Issue Tracker:https://github.com/scoute-dich/Diaspora/issues
Changelog:https://github.com/scoute-dich/Diaspora/blob/HEAD/CHANGELOG.md

Auto Name:Diaspora
Summary:Client for the Diaspora social network
Description:
Unofficial webview based client for the community-run, distributed social
network "Diaspora". It's currently under development and should be used with
that in mind. Please submit any bugs you might find.

Based on [[ar.com.tristeslostrestigres.diasporanativewebapp]].

[https://github.com/scoute-dich/Diaspora/blob/HEAD/SCREENSHOTS.md Screenshots]
.

Repo Type:git
Repo:https://github.com/scoute-dich/Diaspora

Build:1.0.1,2
    disable=https://github.com/scoute-dich/Diaspora/issues/2
    commit=v1.0.1
    subdir=app
    gradle=yes

Build:1.1,5
    disable=https://github.com/scoute-dich/Diaspora/issues/2
    commit=v1.1
    subdir=app
    gradle=yes
    prebuild=echo -e "android { lintOptions { disable 'MissingTranslation' } }" >> build.gradle

Build:1.1.1,10
    disable=https://github.com/scoute-dich/Diaspora/issues/2
    commit=v1.1.1
    subdir=app
    gradle=yes

Build:1.2,15
    disable=https://github.com/scoute-dich/Diaspora/issues/2
    commit=v1.2
    subdir=app
    gradle=yes

Build:1.2.1,16
    disable=https://github.com/scoute-dich/Diaspora/issues/2
    commit=v1.2.1
    subdir=app
    gradle=yes

Build:1.3,17
    commit=v1.3
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.3
Current Version Code:17
