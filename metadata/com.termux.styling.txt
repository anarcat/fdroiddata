Categories:Development
License:GPLv3
Web Site:http://termux.com
Source Code:https://github.com/termux/termux-styling
Issue Tracker:https://github.com/termux/termux-styling/issues

Auto Name:Termux:Styling
Summary:Customize your Termux terminal
Description:
This plugin for [[com.termux]] provides beautiful color schemes and
powerline-ready fonts to customize the appearance of the terminal.

Long-press anywhere on the Termux terminal and use the "Style" menu entry to use
after installation.
.

Repo Type:git
Repo:https://github.com/termux/termux-styling

Build:0.10,10
    commit=v0.10
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.10
Current Version Code:10
