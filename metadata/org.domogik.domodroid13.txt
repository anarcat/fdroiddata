Categories:System
License:Unlicensed
Web Site:http://www.domogik.org
Source Code:https://github.com/domogik/domodroid
Issue Tracker:https://github.com/domogik/domodroid/issues
Changelog:https://github.com/domogik/domodroid/blob/HEAD/Domodroid/src/main/res/raw/changelog.txt

Auto Name:Domodroid
Summary:Interface with a Domogik home automation server
Description:
Interface with a [http://www.domogik.org/ Domogik] home automation server.
.

Repo Type:git
Repo:https://github.com/domogik/domodroid

Build:1.4,15
    disable=missing build files, missing license
    commit=b550267c4b88608609ac173b742de1db0f9512e4
    subdir=Domodroid
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.4
Current Version Code:15
