Categories:Science & Education
License:GPLv3
Web Site:https://github.com/caarmen/poet-assistant/blob/HEAD/README.md
Source Code:https://github.com/caarmen/poet-assistant
Issue Tracker:https://github.com/caarmen/poet-assistant/issues
Changelog:https://github.com/caarmen/poet-assistant/blob/HEAD/CHANGELOG.md

Auto Name:Poet Assistant
Summary:Dictionary and TTS tools for editing poems
Description:
A set of tools to help with writing poems:

* a rhyming dictionary, using the Carnegie Mellon University pronunciation dictionary.
* a thesaurus, using the WordNet thesaurus.
* a dictionary, using the WordNet dictionary.
* a screen where you can enter your poem text, and have it read aloud back to you by the device's text-to-speech engine.
.

Repo Type:git
Repo:https://github.com/caarmen/poet-assistant.git

Build:1.0.1,101
    commit=release-1.0.1
    subdir=app
    gradle=yes

Build:1.0.4,104
    commit=release-1.0.4
    subdir=app
    gradle=yes

Build:1.0.5,105
    commit=release-1.0.5
    subdir=app
    gradle=yes

Build:1.0.6,106
    commit=release-1.0.6
    subdir=app
    gradle=yes

Build:1.0.7,107
    commit=release-1.0.7
    subdir=app
    gradle=yes

Build:1.2.0,120
    commit=release-1.2.0
    subdir=app
    gradle=yes

Build:1.3.0,130
    commit=release-1.3.0
    subdir=app
    gradle=yes

Build:1.4.0,140
    commit=release-1.4.0
    subdir=app
    gradle=yes

Build:1.5.0,150
    commit=release-1.5.0
    subdir=app
    gradle=yes

Auto Update Mode:Version release-%v
Update Check Mode:Tags ^release-
Current Version:1.5.0
Current Version Code:150
