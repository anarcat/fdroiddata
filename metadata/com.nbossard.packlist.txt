Categories:Writing
License:Apache2
Web Site:https://github.com/nbossard/packlist/blob/HEAD/README.md
Source Code:https://github.com/nbossard/packlist
Issue Tracker:https://github.com/nbossard/packlist/issues

Auto Name:packlist
Summary:Manage packing lists
Description:
Helps in planning trips and organizing packing lists.
.

Repo Type:git
Repo:https://github.com/nbossard/packlist

Build:0.6,7
    commit=e42378749705fe90f7ac1f8a37a7cfb573c0b8cf
    subdir=app
    gradle=yes
    rm=gradle.properties

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.7
Current Version Code:8
